﻿using System;
using System.Windows.Forms;

namespace EnigmaMachine
{
    public partial class Form1 : Form
    {
		private Enigma	EnigmaI = new Enigma();
		private bool    UserChange = false;

		public Form1()
        {
            InitializeComponent();
			EnigmaI.PlugClearEvent += new PlugboardEvent( ResetPlugCon );
			EnigmaI.PlugSetEvent += new PlugboardEvent( SetPlugCon );

			RotorSelectionSlow.SelectedIndex = 0;
			RotorSelectionMiddle.SelectedIndex = 1;
			RotorSelectionFast.SelectedIndex = 2;
			ReflectionRotor.SelectedIndex = 0;
			UserChange = true;
        }

		private void ResetPlugCon( object source, PlugboardEventArgs Args )
		{
			GetPlugByChar( Args.ResetChar0 ).Text = null;
			GetPlugByChar( Args.ResetChar1 ).Text = null;
		}

		private void SetPlugCon( object source, PlugboardEventArgs Args )
		{
			GetPlugByChar( Args.ResetChar1 ).Text = Args.ResetChar0.ToString();
			GetPlugByChar( Args.ResetChar0 ).Text = Args.ResetChar1.ToString();
		}

		private MaskedTextBox GetPlugByChar( char Input )
		{
			switch( Input )
			{
				case 'A' : return Plugboard_A;
				case 'B' : return Plugboard_B;
				case 'C' : return Plugboard_C;
				case 'D' : return Plugboard_D;
				case 'E' : return Plugboard_E;
				case 'F' : return Plugboard_F;
				case 'G' : return Plugboard_G;
				case 'H' : return Plugboard_H;
				case 'I' : return Plugboard_I;
				case 'J' : return Plugboard_J;
				case 'K' : return Plugboard_K;
				case 'L' : return Plugboard_L;
				case 'M' : return Plugboard_M;
				case 'N' : return Plugboard_N;
				case 'O' : return Plugboard_O;
				case 'P' : return Plugboard_P;
				case 'Q' : return Plugboard_Q;
				case 'R' : return Plugboard_R;
				case 'S' : return Plugboard_S;
				case 'T' : return Plugboard_T;
				case 'U' : return Plugboard_U;
				case 'V' : return Plugboard_V;
				case 'W' : return Plugboard_W;
				case 'X' : return Plugboard_X;
				case 'Y' : return Plugboard_Y;
				case 'Z' : return Plugboard_Z;
			}
			return null;
		}

		private void PlugTextChanged( object sender, EventArgs e )
		{
			if( UserChange )
			{
				UserChange = false;

				MaskedTextBox Plugboard_Entry = (MaskedTextBox)sender;
				if( Plugboard_Entry.Text != null && Plugboard_Entry.Text.Length == 1 &&
					EnigmaI.ValidChar( Plugboard_Entry.Text[0] ) )
				{
					EnigmaI.SetPlugboardConnection( Plugboard_Entry.PromptChar, char.ToUpper( Plugboard_Entry.Text[0] ) );
				}
				else EnigmaI.SetPlugboardConnection( Plugboard_Entry.PromptChar, Plugboard_Entry.PromptChar ); // Clear Entry

				UserChange = true;
				InputBox_TextChanged( null, null );
			}
		}

		private void RotorStartPositionChanged( object sender, EventArgs e )
		{
			if( UserChange )
			{
				UserChange = false;

				MaskedTextBox RotorStart = (MaskedTextBox)sender;
				if( RotorStart.Text != null && RotorStart.Text.Length == 1 &&
					EnigmaI.ValidChar( RotorStart.Text[0] ) )
				{
					EnigmaI.SetStartChar( RotorStart.Tag, char.ToUpper( RotorStart.Text[0] ) );
				}
				else EnigmaI.SetStartChar( RotorStart.Tag, RotorStart.PromptChar );

				UserChange = true;
				InputBox_TextChanged( null, null );
			}
		}

		private void RotorRatchetChanged( object sender, EventArgs e )
		{
			if( UserChange )
			{
				UserChange = false;

				MaskedTextBox RotorRatchet = (MaskedTextBox)sender;
				if( RotorRatchet.Text != null && RotorRatchet.Text.Length == 1 &&
					EnigmaI.ValidChar( RotorRatchet.Text[0] ) )
				{
					EnigmaI.SetRingStartChar( RotorRatchet.Tag, char.ToUpper( RotorRatchet.Text[0] ) );
				}
				else EnigmaI.SetRingStartChar( RotorRatchet.Tag, RotorRatchet.PromptChar );

				UserChange = true;
				InputBox_TextChanged( null, null );
			}
		}

		private int GetFreeIndex( ComboBox Source, int Unavaiable )
		{
			for( int i = 0; i < 5; ++i )
			{
				if( i == Unavaiable ) continue;
				if( i != Source.SelectedIndex ) return i;
			}
			return 0;
		}

		private void ReflectorIDChanged( object sender, EventArgs e )
		{
			EnigmaI.SetupReflectionRole( ReflectionRotor.SelectedIndex );
			InputBox_TextChanged( null, null );
		}

		private void RotorIDChanged( object sender, EventArgs e )
		{
			if( UserChange )
			{
				UserChange = false;

				ComboBox RotorSelection = (ComboBox)sender;
				ComboBox Target0 = RotorSelectionSlow, Target1 = RotorSelectionMiddle;
				EnigmaI.SetupRole( RotorSelection.Tag, RotorSelection.SelectedIndex );

				if( RotorSelection == RotorSelectionSlow ) Target0 = RotorSelectionFast;
				else if( RotorSelection == RotorSelectionMiddle ) Target1 = RotorSelectionFast;

				// Check if newtarget is used by other rotor
				if( Target0.SelectedIndex == RotorSelection.SelectedIndex )
				{
					Target0.SelectedIndex = GetFreeIndex( Target1, RotorSelection.SelectedIndex );
					EnigmaI.SetupRole( Target0.Tag, Target0.SelectedIndex );
				}
				else if( Target1.SelectedIndex == RotorSelection.SelectedIndex )
				{
					Target1.SelectedIndex = GetFreeIndex( Target0, RotorSelection.SelectedIndex );
					EnigmaI.SetupRole( Target1.Tag, Target1.SelectedIndex );
				}

				UserChange = true;
				InputBox_TextChanged( null, null );
			}
		}

		private void InputBox_TextChanged( object sender, EventArgs e )
		{
			EnigmaI.Reset();

			char[] Output = new char[InputBox.Text.Length];
			int Counter = 0;
			foreach( char c in InputBox.Text )
			{
				if( EnigmaI.ValidChar( c ) )
				{
					Output[Counter++] = EnigmaI.Encode( char.ToUpper( c ) );
				}
			}

			OutputBox.Text = new string( Output );
		}
	}
}
