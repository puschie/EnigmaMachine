﻿namespace EnigmaMachine
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.Label label1;
			System.Windows.Forms.Label label2;
			System.Windows.Forms.Label label3;
			System.Windows.Forms.Label label4;
			System.Windows.Forms.Label label5;
			System.Windows.Forms.Label label6;
			this.Plugboard_A = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_C = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_B = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_D = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_E = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_F = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_G = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_H = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_I = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_J = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_K = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_L = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_M = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_N = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_O = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_P = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_Q = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_R = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_S = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_T = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_U = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_V = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_W = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_X = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_Y = new System.Windows.Forms.MaskedTextBox();
			this.Plugboard_Z = new System.Windows.Forms.MaskedTextBox();
			this.RotorSelectionSlow = new System.Windows.Forms.ComboBox();
			this.RotorSelectionMiddle = new System.Windows.Forms.ComboBox();
			this.RotorSelectionFast = new System.Windows.Forms.ComboBox();
			this.SlowRing = new System.Windows.Forms.MaskedTextBox();
			this.MiddleRing = new System.Windows.Forms.MaskedTextBox();
			this.FastRing = new System.Windows.Forms.MaskedTextBox();
			this.SlowStart = new System.Windows.Forms.MaskedTextBox();
			this.MiddleStart = new System.Windows.Forms.MaskedTextBox();
			this.FastStart = new System.Windows.Forms.MaskedTextBox();
			this.RotorSelection = new System.Windows.Forms.ToolTip(this.components);
			this.ReflectionRotor = new System.Windows.Forms.ComboBox();
			this.RingSelection = new System.Windows.Forms.ToolTip(this.components);
			this.RotorStartSelection = new System.Windows.Forms.ToolTip(this.components);
			this.InputBox = new System.Windows.Forms.TextBox();
			this.OutputBox = new System.Windows.Forms.TextBox();
			label1 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			label3 = new System.Windows.Forms.Label();
			label4 = new System.Windows.Forms.Label();
			label5 = new System.Windows.Forms.Label();
			label6 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label1.Location = new System.Drawing.Point(12, 96);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(92, 23);
			label1.TabIndex = 0;
			label1.Text = "Slow Rot";
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label2.Location = new System.Drawing.Point(180, 96);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(112, 23);
			label2.TabIndex = 0;
			label2.Text = "Middle Rot";
			// 
			// label3
			// 
			label3.AutoSize = true;
			label3.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label3.Location = new System.Drawing.Point(360, 96);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(87, 23);
			label3.TabIndex = 0;
			label3.Text = "Fast Rot";
			// 
			// label4
			// 
			label4.AutoSize = true;
			label4.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label4.Location = new System.Drawing.Point(12, 180);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(95, 23);
			label4.TabIndex = 0;
			label4.Text = "Reflector";
			// 
			// label5
			// 
			label5.AutoSize = true;
			label5.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label5.Location = new System.Drawing.Point(12, 228);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(56, 23);
			label5.TabIndex = 42;
			label5.Text = "Input";
			// 
			// label6
			// 
			label6.AutoSize = true;
			label6.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			label6.Location = new System.Drawing.Point(12, 324);
			label6.Name = "label6";
			label6.Size = new System.Drawing.Size(73, 23);
			label6.TabIndex = 43;
			label6.Text = "Output";
			// 
			// Plugboard_A
			// 
			this.Plugboard_A.AsciiOnly = true;
			this.Plugboard_A.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_A.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_A.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_A.Location = new System.Drawing.Point(12, 12);
			this.Plugboard_A.Mask = "A";
			this.Plugboard_A.Name = "Plugboard_A";
			this.Plugboard_A.PromptChar = 'A';
			this.Plugboard_A.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_A.TabIndex = 0;
			this.Plugboard_A.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_A.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_C
			// 
			this.Plugboard_C.AsciiOnly = true;
			this.Plugboard_C.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_C.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_C.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_C.Location = new System.Drawing.Point(84, 12);
			this.Plugboard_C.Mask = "A";
			this.Plugboard_C.Name = "Plugboard_C";
			this.Plugboard_C.PromptChar = 'C';
			this.Plugboard_C.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_C.TabIndex = 2;
			this.Plugboard_C.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_C.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_B
			// 
			this.Plugboard_B.AsciiOnly = true;
			this.Plugboard_B.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_B.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_B.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_B.Location = new System.Drawing.Point(48, 12);
			this.Plugboard_B.Mask = "A";
			this.Plugboard_B.Name = "Plugboard_B";
			this.Plugboard_B.PromptChar = 'B';
			this.Plugboard_B.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_B.TabIndex = 1;
			this.Plugboard_B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_B.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_D
			// 
			this.Plugboard_D.AsciiOnly = true;
			this.Plugboard_D.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_D.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_D.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_D.Location = new System.Drawing.Point(120, 12);
			this.Plugboard_D.Mask = "A";
			this.Plugboard_D.Name = "Plugboard_D";
			this.Plugboard_D.PromptChar = 'D';
			this.Plugboard_D.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_D.TabIndex = 3;
			this.Plugboard_D.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_D.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_E
			// 
			this.Plugboard_E.AsciiOnly = true;
			this.Plugboard_E.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_E.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_E.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_E.Location = new System.Drawing.Point(156, 12);
			this.Plugboard_E.Mask = "A";
			this.Plugboard_E.Name = "Plugboard_E";
			this.Plugboard_E.PromptChar = 'E';
			this.Plugboard_E.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_E.TabIndex = 4;
			this.Plugboard_E.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_E.TextAlignChanged += new System.EventHandler(this.PlugTextChanged);
			this.Plugboard_E.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_F
			// 
			this.Plugboard_F.AsciiOnly = true;
			this.Plugboard_F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_F.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_F.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_F.Location = new System.Drawing.Point(192, 12);
			this.Plugboard_F.Mask = "A";
			this.Plugboard_F.Name = "Plugboard_F";
			this.Plugboard_F.PromptChar = 'F';
			this.Plugboard_F.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_F.TabIndex = 5;
			this.Plugboard_F.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_F.TextAlignChanged += new System.EventHandler(this.PlugTextChanged);
			this.Plugboard_F.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_G
			// 
			this.Plugboard_G.AsciiOnly = true;
			this.Plugboard_G.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_G.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_G.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_G.Location = new System.Drawing.Point(228, 12);
			this.Plugboard_G.Mask = "A";
			this.Plugboard_G.Name = "Plugboard_G";
			this.Plugboard_G.PromptChar = 'G';
			this.Plugboard_G.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_G.TabIndex = 6;
			this.Plugboard_G.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_G.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_H
			// 
			this.Plugboard_H.AsciiOnly = true;
			this.Plugboard_H.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_H.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_H.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_H.Location = new System.Drawing.Point(264, 12);
			this.Plugboard_H.Mask = "A";
			this.Plugboard_H.Name = "Plugboard_H";
			this.Plugboard_H.PromptChar = 'H';
			this.Plugboard_H.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_H.TabIndex = 7;
			this.Plugboard_H.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_H.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_I
			// 
			this.Plugboard_I.AsciiOnly = true;
			this.Plugboard_I.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_I.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_I.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_I.Location = new System.Drawing.Point(300, 12);
			this.Plugboard_I.Mask = "A";
			this.Plugboard_I.Name = "Plugboard_I";
			this.Plugboard_I.PromptChar = 'I';
			this.Plugboard_I.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_I.TabIndex = 8;
			this.Plugboard_I.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_I.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_J
			// 
			this.Plugboard_J.AsciiOnly = true;
			this.Plugboard_J.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_J.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_J.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_J.Location = new System.Drawing.Point(336, 12);
			this.Plugboard_J.Mask = "A";
			this.Plugboard_J.Name = "Plugboard_J";
			this.Plugboard_J.PromptChar = 'J';
			this.Plugboard_J.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_J.TabIndex = 9;
			this.Plugboard_J.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_J.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_K
			// 
			this.Plugboard_K.AsciiOnly = true;
			this.Plugboard_K.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_K.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_K.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_K.Location = new System.Drawing.Point(372, 12);
			this.Plugboard_K.Mask = "A";
			this.Plugboard_K.Name = "Plugboard_K";
			this.Plugboard_K.PromptChar = 'K';
			this.Plugboard_K.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_K.TabIndex = 10;
			this.Plugboard_K.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_K.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_L
			// 
			this.Plugboard_L.AsciiOnly = true;
			this.Plugboard_L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_L.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_L.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_L.Location = new System.Drawing.Point(408, 12);
			this.Plugboard_L.Mask = "A";
			this.Plugboard_L.Name = "Plugboard_L";
			this.Plugboard_L.PromptChar = 'L';
			this.Plugboard_L.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_L.TabIndex = 11;
			this.Plugboard_L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_L.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_M
			// 
			this.Plugboard_M.AsciiOnly = true;
			this.Plugboard_M.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_M.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_M.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_M.Location = new System.Drawing.Point(444, 12);
			this.Plugboard_M.Mask = "A";
			this.Plugboard_M.Name = "Plugboard_M";
			this.Plugboard_M.PromptChar = 'M';
			this.Plugboard_M.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_M.TabIndex = 12;
			this.Plugboard_M.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_M.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_N
			// 
			this.Plugboard_N.AsciiOnly = true;
			this.Plugboard_N.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_N.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_N.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_N.Location = new System.Drawing.Point(12, 48);
			this.Plugboard_N.Mask = "A";
			this.Plugboard_N.Name = "Plugboard_N";
			this.Plugboard_N.PromptChar = 'N';
			this.Plugboard_N.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_N.TabIndex = 13;
			this.Plugboard_N.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_N.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_O
			// 
			this.Plugboard_O.AsciiOnly = true;
			this.Plugboard_O.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_O.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_O.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_O.Location = new System.Drawing.Point(48, 48);
			this.Plugboard_O.Mask = "A";
			this.Plugboard_O.Name = "Plugboard_O";
			this.Plugboard_O.PromptChar = 'O';
			this.Plugboard_O.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_O.TabIndex = 14;
			this.Plugboard_O.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_O.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_P
			// 
			this.Plugboard_P.AsciiOnly = true;
			this.Plugboard_P.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_P.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_P.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_P.Location = new System.Drawing.Point(84, 48);
			this.Plugboard_P.Mask = "A";
			this.Plugboard_P.Name = "Plugboard_P";
			this.Plugboard_P.PromptChar = 'P';
			this.Plugboard_P.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_P.TabIndex = 15;
			this.Plugboard_P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_P.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_Q
			// 
			this.Plugboard_Q.AsciiOnly = true;
			this.Plugboard_Q.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_Q.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_Q.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_Q.Location = new System.Drawing.Point(120, 48);
			this.Plugboard_Q.Mask = "A";
			this.Plugboard_Q.Name = "Plugboard_Q";
			this.Plugboard_Q.PromptChar = 'Q';
			this.Plugboard_Q.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_Q.TabIndex = 16;
			this.Plugboard_Q.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_Q.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_R
			// 
			this.Plugboard_R.AsciiOnly = true;
			this.Plugboard_R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_R.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_R.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_R.Location = new System.Drawing.Point(156, 48);
			this.Plugboard_R.Mask = "A";
			this.Plugboard_R.Name = "Plugboard_R";
			this.Plugboard_R.PromptChar = 'R';
			this.Plugboard_R.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_R.TabIndex = 17;
			this.Plugboard_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_R.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_S
			// 
			this.Plugboard_S.AsciiOnly = true;
			this.Plugboard_S.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_S.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_S.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_S.Location = new System.Drawing.Point(192, 48);
			this.Plugboard_S.Mask = "A";
			this.Plugboard_S.Name = "Plugboard_S";
			this.Plugboard_S.PromptChar = 'S';
			this.Plugboard_S.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_S.TabIndex = 18;
			this.Plugboard_S.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_S.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_T
			// 
			this.Plugboard_T.AsciiOnly = true;
			this.Plugboard_T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_T.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_T.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_T.Location = new System.Drawing.Point(228, 48);
			this.Plugboard_T.Mask = "A";
			this.Plugboard_T.Name = "Plugboard_T";
			this.Plugboard_T.PromptChar = 'T';
			this.Plugboard_T.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_T.TabIndex = 19;
			this.Plugboard_T.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_T.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_U
			// 
			this.Plugboard_U.AsciiOnly = true;
			this.Plugboard_U.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_U.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_U.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_U.Location = new System.Drawing.Point(264, 48);
			this.Plugboard_U.Mask = "A";
			this.Plugboard_U.Name = "Plugboard_U";
			this.Plugboard_U.PromptChar = 'U';
			this.Plugboard_U.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_U.TabIndex = 20;
			this.Plugboard_U.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_U.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_V
			// 
			this.Plugboard_V.AsciiOnly = true;
			this.Plugboard_V.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_V.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_V.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_V.Location = new System.Drawing.Point(300, 48);
			this.Plugboard_V.Mask = "A";
			this.Plugboard_V.Name = "Plugboard_V";
			this.Plugboard_V.PromptChar = 'V';
			this.Plugboard_V.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_V.TabIndex = 21;
			this.Plugboard_V.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_V.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_W
			// 
			this.Plugboard_W.AsciiOnly = true;
			this.Plugboard_W.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_W.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_W.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_W.Location = new System.Drawing.Point(336, 48);
			this.Plugboard_W.Mask = "A";
			this.Plugboard_W.Name = "Plugboard_W";
			this.Plugboard_W.PromptChar = 'W';
			this.Plugboard_W.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_W.TabIndex = 22;
			this.Plugboard_W.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_W.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_X
			// 
			this.Plugboard_X.AsciiOnly = true;
			this.Plugboard_X.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_X.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_X.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_X.Location = new System.Drawing.Point(372, 48);
			this.Plugboard_X.Mask = "A";
			this.Plugboard_X.Name = "Plugboard_X";
			this.Plugboard_X.PromptChar = 'X';
			this.Plugboard_X.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_X.TabIndex = 23;
			this.Plugboard_X.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_X.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_Y
			// 
			this.Plugboard_Y.AsciiOnly = true;
			this.Plugboard_Y.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_Y.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_Y.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_Y.Location = new System.Drawing.Point(408, 48);
			this.Plugboard_Y.Mask = "A";
			this.Plugboard_Y.Name = "Plugboard_Y";
			this.Plugboard_Y.PromptChar = 'Y';
			this.Plugboard_Y.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_Y.TabIndex = 24;
			this.Plugboard_Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_Y.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// Plugboard_Z
			// 
			this.Plugboard_Z.AsciiOnly = true;
			this.Plugboard_Z.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Plugboard_Z.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Plugboard_Z.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.Plugboard_Z.Location = new System.Drawing.Point(444, 48);
			this.Plugboard_Z.Mask = "A";
			this.Plugboard_Z.Name = "Plugboard_Z";
			this.Plugboard_Z.PromptChar = 'Z';
			this.Plugboard_Z.Size = new System.Drawing.Size(22, 26);
			this.Plugboard_Z.TabIndex = 25;
			this.Plugboard_Z.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.Plugboard_Z.TextChanged += new System.EventHandler(this.PlugTextChanged);
			// 
			// RotorSelectionSlow
			// 
			this.RotorSelectionSlow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.RotorSelectionSlow.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.RotorSelectionSlow.FormattingEnabled = true;
			this.RotorSelectionSlow.Items.AddRange(new object[] {
            "I",
            "II",
            "III",
            "IV",
            "V"});
			this.RotorSelectionSlow.Location = new System.Drawing.Point(12, 132);
			this.RotorSelectionSlow.Name = "RotorSelectionSlow";
			this.RotorSelectionSlow.Size = new System.Drawing.Size(36, 26);
			this.RotorSelectionSlow.TabIndex = 26;
			this.RotorSelectionSlow.Tag = "0";
			this.RotorSelection.SetToolTip(this.RotorSelectionSlow, "Select one of five pre-defined rotors");
			this.RotorSelectionSlow.SelectedValueChanged += new System.EventHandler(this.RotorIDChanged);
			// 
			// RotorSelectionMiddle
			// 
			this.RotorSelectionMiddle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.RotorSelectionMiddle.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.RotorSelectionMiddle.FormattingEnabled = true;
			this.RotorSelectionMiddle.Items.AddRange(new object[] {
            "I",
            "II",
            "III",
            "IV",
            "V"});
			this.RotorSelectionMiddle.Location = new System.Drawing.Point(180, 132);
			this.RotorSelectionMiddle.Name = "RotorSelectionMiddle";
			this.RotorSelectionMiddle.Size = new System.Drawing.Size(36, 26);
			this.RotorSelectionMiddle.TabIndex = 29;
			this.RotorSelectionMiddle.Tag = "1";
			this.RotorSelection.SetToolTip(this.RotorSelectionMiddle, "Select one of five pre-defined rotors");
			this.RotorSelectionMiddle.SelectedValueChanged += new System.EventHandler(this.RotorIDChanged);
			// 
			// RotorSelectionFast
			// 
			this.RotorSelectionFast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.RotorSelectionFast.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.RotorSelectionFast.FormattingEnabled = true;
			this.RotorSelectionFast.Items.AddRange(new object[] {
            "I",
            "II",
            "III",
            "IV",
            "V"});
			this.RotorSelectionFast.Location = new System.Drawing.Point(360, 132);
			this.RotorSelectionFast.Name = "RotorSelectionFast";
			this.RotorSelectionFast.Size = new System.Drawing.Size(36, 26);
			this.RotorSelectionFast.TabIndex = 32;
			this.RotorSelectionFast.Tag = "2";
			this.RotorSelection.SetToolTip(this.RotorSelectionFast, "Select one of five pre-defined rotors");
			this.RotorSelectionFast.SelectedValueChanged += new System.EventHandler(this.RotorIDChanged);
			// 
			// SlowRing
			// 
			this.SlowRing.AsciiOnly = true;
			this.SlowRing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.SlowRing.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SlowRing.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.SlowRing.Location = new System.Drawing.Point(60, 132);
			this.SlowRing.Mask = "A";
			this.SlowRing.Name = "SlowRing";
			this.SlowRing.PromptChar = 'A';
			this.SlowRing.Size = new System.Drawing.Size(22, 26);
			this.SlowRing.TabIndex = 27;
			this.SlowRing.Tag = "0";
			this.SlowRing.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.RingSelection.SetToolTip(this.SlowRing, "Select the ring start");
			this.SlowRing.TextChanged += new System.EventHandler(this.RotorRatchetChanged);
			// 
			// MiddleRing
			// 
			this.MiddleRing.AsciiOnly = true;
			this.MiddleRing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.MiddleRing.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MiddleRing.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.MiddleRing.Location = new System.Drawing.Point(228, 132);
			this.MiddleRing.Mask = "A";
			this.MiddleRing.Name = "MiddleRing";
			this.MiddleRing.PromptChar = 'A';
			this.MiddleRing.Size = new System.Drawing.Size(22, 26);
			this.MiddleRing.TabIndex = 30;
			this.MiddleRing.Tag = "1";
			this.MiddleRing.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.RingSelection.SetToolTip(this.MiddleRing, "Select the ring start");
			this.MiddleRing.TextChanged += new System.EventHandler(this.RotorRatchetChanged);
			// 
			// FastRing
			// 
			this.FastRing.AsciiOnly = true;
			this.FastRing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.FastRing.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FastRing.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.FastRing.Location = new System.Drawing.Point(408, 132);
			this.FastRing.Mask = "A";
			this.FastRing.Name = "FastRing";
			this.FastRing.PromptChar = 'A';
			this.FastRing.Size = new System.Drawing.Size(22, 26);
			this.FastRing.TabIndex = 33;
			this.FastRing.Tag = "2";
			this.FastRing.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.RingSelection.SetToolTip(this.FastRing, "Select the ring start");
			this.FastRing.TextChanged += new System.EventHandler(this.RotorRatchetChanged);
			// 
			// SlowStart
			// 
			this.SlowStart.AsciiOnly = true;
			this.SlowStart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.SlowStart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SlowStart.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.SlowStart.Location = new System.Drawing.Point(96, 132);
			this.SlowStart.Mask = "A";
			this.SlowStart.Name = "SlowStart";
			this.SlowStart.PromptChar = 'A';
			this.SlowStart.Size = new System.Drawing.Size(22, 26);
			this.SlowStart.TabIndex = 28;
			this.SlowStart.Tag = "0";
			this.SlowStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.RotorStartSelection.SetToolTip(this.SlowStart, "Select the rotor start position");
			this.SlowStart.TextChanged += new System.EventHandler(this.RotorStartPositionChanged);
			// 
			// MiddleStart
			// 
			this.MiddleStart.AsciiOnly = true;
			this.MiddleStart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.MiddleStart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.MiddleStart.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.MiddleStart.Location = new System.Drawing.Point(264, 132);
			this.MiddleStart.Mask = "A";
			this.MiddleStart.Name = "MiddleStart";
			this.MiddleStart.PromptChar = 'A';
			this.MiddleStart.Size = new System.Drawing.Size(22, 26);
			this.MiddleStart.TabIndex = 31;
			this.MiddleStart.Tag = "1";
			this.MiddleStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.RotorStartSelection.SetToolTip(this.MiddleStart, "Select the rotor start position");
			this.MiddleStart.TextChanged += new System.EventHandler(this.RotorStartPositionChanged);
			// 
			// FastStart
			// 
			this.FastStart.AsciiOnly = true;
			this.FastStart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.FastStart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FastStart.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
			this.FastStart.Location = new System.Drawing.Point(444, 132);
			this.FastStart.Mask = "A";
			this.FastStart.Name = "FastStart";
			this.FastStart.PromptChar = 'A';
			this.FastStart.Size = new System.Drawing.Size(22, 26);
			this.FastStart.TabIndex = 34;
			this.FastStart.Tag = "2";
			this.FastStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.RotorStartSelection.SetToolTip(this.FastStart, "Select the rotor start position");
			this.FastStart.TextChanged += new System.EventHandler(this.RotorStartPositionChanged);
			// 
			// RotorSelection
			// 
			this.RotorSelection.ToolTipTitle = "Rotor";
			// 
			// ReflectionRotor
			// 
			this.ReflectionRotor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ReflectionRotor.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ReflectionRotor.FormattingEnabled = true;
			this.ReflectionRotor.Items.AddRange(new object[] {
            "I",
            "II",
            "III"});
			this.ReflectionRotor.Location = new System.Drawing.Point(120, 180);
			this.ReflectionRotor.Name = "ReflectionRotor";
			this.ReflectionRotor.Size = new System.Drawing.Size(36, 26);
			this.ReflectionRotor.TabIndex = 35;
			this.ReflectionRotor.Tag = "4";
			this.RotorSelection.SetToolTip(this.ReflectionRotor, "Select one of 3 pre-defined reflector-rotors");
			this.ReflectionRotor.TextChanged += new System.EventHandler(this.ReflectorIDChanged);
			// 
			// RingSelection
			// 
			this.RingSelection.ToolTipTitle = "Ring";
			// 
			// RotorStartSelection
			// 
			this.RotorStartSelection.ToolTipTitle = "Start Position";
			// 
			// InputBox
			// 
			this.InputBox.Location = new System.Drawing.Point(12, 252);
			this.InputBox.MaxLength = 300;
			this.InputBox.Name = "InputBox";
			this.InputBox.Size = new System.Drawing.Size(456, 20);
			this.InputBox.TabIndex = 36;
			this.InputBox.WordWrap = false;
			this.InputBox.TextChanged += new System.EventHandler(this.InputBox_TextChanged);
			// 
			// OutputBox
			// 
			this.OutputBox.Location = new System.Drawing.Point(12, 348);
			this.OutputBox.MaxLength = 300;
			this.OutputBox.Name = "OutputBox";
			this.OutputBox.ReadOnly = true;
			this.OutputBox.Size = new System.Drawing.Size(456, 20);
			this.OutputBox.TabIndex = 37;
			this.OutputBox.TabStop = false;
			this.OutputBox.WordWrap = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.ClientSize = new System.Drawing.Size(481, 377);
			this.Controls.Add(label6);
			this.Controls.Add(label5);
			this.Controls.Add(this.OutputBox);
			this.Controls.Add(this.InputBox);
			this.Controls.Add(this.ReflectionRotor);
			this.Controls.Add(this.FastStart);
			this.Controls.Add(this.MiddleStart);
			this.Controls.Add(this.SlowStart);
			this.Controls.Add(this.FastRing);
			this.Controls.Add(this.MiddleRing);
			this.Controls.Add(this.SlowRing);
			this.Controls.Add(label4);
			this.Controls.Add(this.RotorSelectionFast);
			this.Controls.Add(this.RotorSelectionMiddle);
			this.Controls.Add(this.RotorSelectionSlow);
			this.Controls.Add(label3);
			this.Controls.Add(label2);
			this.Controls.Add(label1);
			this.Controls.Add(this.Plugboard_Z);
			this.Controls.Add(this.Plugboard_Y);
			this.Controls.Add(this.Plugboard_X);
			this.Controls.Add(this.Plugboard_W);
			this.Controls.Add(this.Plugboard_V);
			this.Controls.Add(this.Plugboard_U);
			this.Controls.Add(this.Plugboard_T);
			this.Controls.Add(this.Plugboard_S);
			this.Controls.Add(this.Plugboard_R);
			this.Controls.Add(this.Plugboard_Q);
			this.Controls.Add(this.Plugboard_P);
			this.Controls.Add(this.Plugboard_O);
			this.Controls.Add(this.Plugboard_N);
			this.Controls.Add(this.Plugboard_M);
			this.Controls.Add(this.Plugboard_L);
			this.Controls.Add(this.Plugboard_K);
			this.Controls.Add(this.Plugboard_J);
			this.Controls.Add(this.Plugboard_I);
			this.Controls.Add(this.Plugboard_H);
			this.Controls.Add(this.Plugboard_G);
			this.Controls.Add(this.Plugboard_F);
			this.Controls.Add(this.Plugboard_E);
			this.Controls.Add(this.Plugboard_D);
			this.Controls.Add(this.Plugboard_B);
			this.Controls.Add(this.Plugboard_C);
			this.Controls.Add(this.Plugboard_A);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Enigma Machine I";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

		#endregion

		private System.Windows.Forms.MaskedTextBox Plugboard_A;
		private System.Windows.Forms.MaskedTextBox Plugboard_C;
		private System.Windows.Forms.MaskedTextBox Plugboard_B;
		private System.Windows.Forms.MaskedTextBox Plugboard_D;
		private System.Windows.Forms.MaskedTextBox Plugboard_E;
		private System.Windows.Forms.MaskedTextBox Plugboard_F;
		private System.Windows.Forms.MaskedTextBox Plugboard_G;
		private System.Windows.Forms.MaskedTextBox Plugboard_H;
		private System.Windows.Forms.MaskedTextBox Plugboard_I;
		private System.Windows.Forms.MaskedTextBox Plugboard_J;
		private System.Windows.Forms.MaskedTextBox Plugboard_K;
		private System.Windows.Forms.MaskedTextBox Plugboard_L;
		private System.Windows.Forms.MaskedTextBox Plugboard_M;
		private System.Windows.Forms.MaskedTextBox Plugboard_N;
		private System.Windows.Forms.MaskedTextBox Plugboard_O;
		private System.Windows.Forms.MaskedTextBox Plugboard_P;
		private System.Windows.Forms.MaskedTextBox Plugboard_Q;
		private System.Windows.Forms.MaskedTextBox Plugboard_R;
		private System.Windows.Forms.MaskedTextBox Plugboard_S;
		private System.Windows.Forms.MaskedTextBox Plugboard_T;
		private System.Windows.Forms.MaskedTextBox Plugboard_U;
		private System.Windows.Forms.MaskedTextBox Plugboard_V;
		private System.Windows.Forms.MaskedTextBox Plugboard_W;
		private System.Windows.Forms.MaskedTextBox Plugboard_X;
		private System.Windows.Forms.MaskedTextBox Plugboard_Y;
		private System.Windows.Forms.MaskedTextBox Plugboard_Z;
		private System.Windows.Forms.ComboBox RotorSelectionSlow;
		private System.Windows.Forms.ComboBox RotorSelectionMiddle;
		private System.Windows.Forms.ComboBox RotorSelectionFast;
		private System.Windows.Forms.ToolTip RotorSelection;
		private System.Windows.Forms.MaskedTextBox SlowRing;
		private System.Windows.Forms.MaskedTextBox MiddleRing;
		private System.Windows.Forms.MaskedTextBox FastRing;
		private System.Windows.Forms.MaskedTextBox SlowStart;
		private System.Windows.Forms.MaskedTextBox MiddleStart;
		private System.Windows.Forms.MaskedTextBox FastStart;
		private System.Windows.Forms.ToolTip RingSelection;
		private System.Windows.Forms.ToolTip RotorStartSelection;
		private System.Windows.Forms.ComboBox ReflectionRotor;
		private System.Windows.Forms.TextBox InputBox;
		private System.Windows.Forms.TextBox OutputBox;
	}
}

