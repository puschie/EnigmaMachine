﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace EnigmaMachine
{
	public class PlugboardEventArgs : EventArgs
	{
		public char ResetChar0;
		public char ResetChar1;
		public PlugboardEventArgs( char Item1, char Item2 )
		{
			ResetChar0 = Item1;
			ResetChar1 = Item2;
		}
	}

	public delegate void PlugboardEvent( object source, PlugboardEventArgs e );

    class Enigma
    {
		public event PlugboardEvent PlugClearEvent;
		public event PlugboardEvent PlugSetEvent;

		private List<Tuple<char, char>>	Plugboard = new List<Tuple<char, char>>();
		private byte[]       RotorStartPosition = new byte[3] { 0, 0, 0 };

        private char[]		ReflectionRole;
		private char[][]    Rotors = new char[3][];
		private byte[]      RotorPosition = new byte[3] { 0, 0, 0 };
		private byte[]      RotorRing = new byte[3] { 0, 0, 0 };
		private byte[]      RotorTurnover = new byte[3] { 0, 0, 0 };

		public void Reset()
		{
			Array.Copy( RotorStartPosition, RotorPosition, 3 );
		}

		public void SetupRole( object RotorNumber, int RoleID )
		{
			int iRotorID = int.Parse( RotorNumber.ToString() );
			Debug.Assert( iRotorID < 3 && iRotorID > -1 );
			Debug.Assert( RoleID < 5 && RoleID > -1 );

			// Turnover are increased by one to omit MOD on each check

			switch( RoleID )
			{
				case 0 :
					Rotors[iRotorID] = new char[26] { 'E', 'K', 'M', 'F', 'L', 'G', 'D', 'Q', 'V', 'Z', 'N', 'T', 'O', 'W', 'Y', 'H', 'X', 'U', 'S', 'P', 'A', 'I', 'B', 'R', 'C', 'J' };
					RotorTurnover[iRotorID] = 16;
					break;

				case 1 :
					Rotors[iRotorID] = new char[26] { 'A', 'J', 'D', 'K', 'S', 'I', 'R', 'U', 'X', 'B', 'L', 'H', 'W', 'T', 'M', 'C', 'Q', 'G', 'Z', 'N', 'P', 'Y', 'F', 'V', 'O', 'E' };
					RotorTurnover[iRotorID] = 4;
					break;

				case 2 :
					Rotors[iRotorID] = new char[26] { 'B', 'D', 'F', 'H', 'J', 'L', 'C', 'P', 'R', 'T', 'X', 'V', 'Z', 'N', 'Y', 'E', 'I', 'W', 'G', 'A', 'K', 'M', 'U', 'S', 'Q', 'O' };
					RotorTurnover[iRotorID] = 21;
					break;

				case 3 :
					Rotors[iRotorID] = new char[26] { 'E', 'S', 'O', 'V', 'P', 'Z', 'J', 'A', 'Y', 'Q', 'U', 'I', 'R', 'H', 'X', 'L', 'N', 'F', 'T', 'G', 'K', 'D', 'C', 'M', 'W', 'B' };
					RotorTurnover[iRotorID] = 9;
					break;

				case 4 :
					Rotors[iRotorID] = new char[26] { 'V', 'Z', 'B', 'R', 'G', 'I', 'T', 'Y', 'U', 'P', 'S', 'D', 'N', 'H', 'L', 'X', 'A', 'W', 'M', 'J', 'Q', 'O', 'F', 'E', 'C', 'K' };
					RotorTurnover[iRotorID] = 25;
					break;
			}

			Reset();
		}

		public void SetupReflectionRole( int RoleID )
		{
			Debug.Assert( RoleID < 3 && RoleID > -1 );

			switch( RoleID )
			{
				case 0 :
					ReflectionRole = new char[26] { 'E', 'J', 'M', 'Z', 'A', 'L', 'Y', 'X', 'V', 'B', 'W', 'F', 'C', 'R', 'Q', 'U', 'O', 'N', 'T', 'S', 'P', 'I', 'K', 'H', 'G', 'D' };
					break;

				case 1 :
					ReflectionRole = new char[26] { 'Y', 'R', 'U', 'H', 'Q', 'S', 'L', 'D', 'P', 'X', 'N', 'G', 'O', 'K', 'M', 'I', 'E', 'B', 'F', 'Z', 'C', 'W', 'V', 'J', 'A', 'T' };
					break;

				case 2 :
					ReflectionRole = new char[26] { 'F', 'V', 'P', 'J', 'I', 'A', 'O', 'Y', 'E', 'D', 'R', 'Z', 'X', 'W', 'G', 'C', 'T', 'K', 'U', 'Q', 'S', 'B', 'N', 'M', 'H', 'L' };
					break;
			}

			Reset();
		}

		private static int MOD( int X ) // fixed mod 26
		{
			return ( X % 26 + 26 ) % 26; // mod for negative numbers
		}

		private char GetPlugboardConnection( char Input )
		{
			Tuple<char,char> FoundedConnection = Plugboard.Find( PlugCon => PlugCon.Item1 == Input || PlugCon.Item2 == Input );
			if( default( Tuple<char, char> ) != FoundedConnection )
			{
				if( FoundedConnection.Item1 == Input ) return FoundedConnection.Item2;
				return FoundedConnection.Item1;
			}
			return Input;
		}

		public char Encode( char InputChar )
		{
			Debug.Assert( ValidChar( InputChar ) );
			
			// Plugboard check
			char TranslatedChar = GetPlugboardConnection( InputChar );
			Debug.WriteLine( "Plugboard : " + InputChar + " -> " + TranslatedChar );

			// Move Rotors
			if( RotorPosition[1] == RotorTurnover[1] )
			{
				RotorPosition[0]++;
				RotorPosition[0] %= 26;

				RotorPosition[1]++;
				RotorPosition[1] %= 26;
			}
			else if( RotorPosition[2] == RotorTurnover[2] )
			{
				RotorPosition[1]++;
				RotorPosition[1] %= 26;
			}

			RotorPosition[2]++;
			RotorPosition[2] %= 26;

			// Apply Position, Offset, Ring and get translated char from each rotor
			int Index = 0;
			for( int i = 2; i > -1; --i )
			{
				Index = MOD( RotorPosition[i] + ( TranslatedChar - 65 ) - RotorRing[i] );
				TranslatedChar = (char)( MOD( RotorRing[i] + ( Rotors[i][Index] - 65 ) - RotorPosition[i] ) + 65 );
				Debug.WriteLine( "Rotor " + i + " : " + (char)( MOD( Index - RotorPosition[i] + RotorRing[i] ) + 65 ) + " = "+ (char)( MOD( Index + RotorRing[i] ) + 65 ) + " -> " + (char)( MOD( ( Rotors[i][Index] - 65 ) - RotorRing[i] ) + 65 ) + " = " + TranslatedChar );
			}

			// Apply Reflector
			Debug.WriteLine( "Reflector : " + TranslatedChar + " -> " + ReflectionRole[( TranslatedChar - 65 )] );
			TranslatedChar = ReflectionRole[( TranslatedChar - 65 )];

			// Backward
			for( int i = 0; i < 3; ++i )
			{
				Index = Array.IndexOf( Rotors[i], (char)( MOD( RotorPosition[i] + ( TranslatedChar - 65 ) - RotorRing[i] ) + 65 ) );
				TranslatedChar = (char)( MOD( Index - RotorPosition[i] + RotorRing[i] ) + 65);
				Debug.WriteLine( "Rotor " + i + " : " + (char)( MOD( ( Rotors[i][Index] - 65 ) + RotorRing[i] - RotorPosition[i] ) + 65 ) + " = " + (char)( MOD( ( Rotors[i][Index] - 65 ) - RotorRing[i] ) + 65 ) + " -> " + (char)( MOD( Index + RotorRing[i] ) + 65 ) + " = " + TranslatedChar );
			}

			// Plugboard check 2
			char FullTranslated = GetPlugboardConnection( TranslatedChar );
			Debug.WriteLine( "Plugboard : " + TranslatedChar + " -> " + FullTranslated );
			Debug.WriteLine("");

			return FullTranslated;
		}

		public bool ValidChar( char Input )
		{
			int AsciiDec = char.ToUpper( Input );
			return ( AsciiDec > 64 && AsciiDec < 91 );
		}

		public void SetStartChar( object RotorNumber, char StartChar )
		{
			int iRotorID = int.Parse( RotorNumber.ToString() );
			Debug.Assert( iRotorID > -1 && iRotorID < 3 );
			RotorStartPosition[iRotorID] = (byte)(StartChar - 65);

			Reset();
		}

		public void SetRingStartChar( object RotorNumber, char RatchtetChar )
		{
			int iRotorID = int.Parse( RotorNumber.ToString() );
			Debug.Assert( iRotorID > -1 && iRotorID < 3 );
			RotorRing[iRotorID] = (byte)( RatchtetChar - 65 );

			Reset();
		}

		private void ThrowEvent( Tuple<char,char> Input )
		{
			if( Input != default( Tuple<char, char> ) )
			{
				PlugClearEvent?.Invoke( this, new PlugboardEventArgs( Input.Item1, Input.Item2 ) );
				Plugboard.Remove( Input );
			}
		}

		public void SetPlugboardConnection( char ID, char Input )
		{
			ThrowEvent( Plugboard.Find( PlugCon => PlugCon.Item1 == Input || PlugCon.Item2 == Input ) );	// Check for used Target																						
			ThrowEvent( Plugboard.Find( PlugCon => PlugCon.Item1 == ID || PlugCon.Item2 == ID ) );			// Check for used Source

			if( ID != Input )
			{
				Plugboard.Add( new Tuple<char, char>( ID, Input ) );
				PlugSetEvent?.Invoke( this, new PlugboardEventArgs( ID, Input ) ); // thorw set-command to show ID on Input MaskedTextField
			}

			Reset();
		}

		public Enigma()
		{
			SetupRole( 0, 0 );
			SetupRole( 1, 1 );
			SetupRole( 2, 2 );
			SetupReflectionRole( 0 );
		}
    }
}
